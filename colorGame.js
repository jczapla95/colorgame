

var winningColor;
var colors = [];
var blocks = document.getElementsByClassName("block");
var message = document.getElementById("message");
var h1 = document.querySelector("h1");
var resetButton = document.getElementById("reset");
var body = document.querySelector("body");
var easyButton = document.getElementById("easy");
var hardButton = document.getElementById("hard");
var winningText = document.getElementById("winningColor");
var originalLength = blocks.length;



easyButton.addEventListener("click", function () {
   easyButton.classList.add("selected");
   hardButton.classList.remove("selected");
   message.textContent = "";
   h1.style.backgroundColor = "steelblue";
   resetButton.textContent = "New Colors";

   for(var i = blocks.length - 1;  i >= originalLength / 2; i--) {
        blocks[i].classList.add("hidden");
        blocks[i].classList.remove("block");
   }
   main();
});

hardButton.addEventListener("click", function () {
    hardButton.classList.add("selected");
    easyButton.classList.remove("selected");
    message.textContent = "";
    h1.style.backgroundColor = "steelblue";
    resetButton.textContent = "New Colors";

    var hiddenBlocks = document.getElementsByClassName("hidden");
    var originalLength = hiddenBlocks.length;
    for(var i = originalLength - 1; i >= 0; i--){
        hiddenBlocks[i].classList.add("block");
        hiddenBlocks[i].classList.remove("hidden");
    }
    main();
});


resetButton.addEventListener("click", function () {
    h1.style.backgroundColor = body.style.backgroundColor;
    resetButton.textContent = "New colors";
    message.textContent = "";
    main();
});

main();

function main(){
    for(var i = 0; i < blocks.length; i++){
        //Managing colors
        createColor(i);
        blocks[i].style.backgroundColor = colors[i];
    }

    var winningIndex = Math.floor(Math.random() * blocks.length);
    winningColor = colors[winningIndex];
    winningText.textContent = winningColor;

    for(i = 0; i < blocks.length; i++){
        blocks[i].addEventListener("click", function(){
            if(this.style.backgroundColor === winningColor){
                changeColors();
                message.textContent = "Correct";
                h1.style.backgroundColor = winningColor;
                resetButton.textContent = "Play again?";
            }
            else{
                this.style.backgroundColor = "#232323";
                message.textContent = "Try Again"
            }
        });
    }
}

function createColor(index){
    var color = "rgb(" + (Math.floor(Math.random() * 255)).toString() + ", " +
        (Math.floor(Math.random() * 255)).toString() + ", " +
        (Math.floor(Math.random() * 255)).toString() + ")";
    colors[index] = color
}

function changeColors(){
    for(var i = 0; i < blocks.length; i++){
        blocks[i].style.backgroundColor = winningColor;
    }
}
